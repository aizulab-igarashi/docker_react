const path = require('path');

module.exports = [
    {
        entry: path.resolve('src/entry/app.js'),
        output: {
            path: path.resolve('dist/js'),
            publicPath: '/js/',
            filename: 'bundle.js'
        },
        devServer: {
            contentBase: 'dist',
            inline: true,
            hot: true
        },
        module: {
            rules: [
                {
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                    query: {
                        presets: ['react', 'es2015']
                    }
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.jsx']
        }
    }
]
