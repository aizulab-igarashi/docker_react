FROM centos
MAINTAINER Gen Igarashi < igarashi@aizulab.com >

# /bin/shを/bin/bashに上書き
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Package
RUN yum update -y
RUN yum -y install epel-release
RUN yum install -y git curl wget unzip make autoconf
RUN yum clean all

# Node.js
# gitでnvmをインストール
ENV NVM_DIR /opt/nvm
ENV NODE_VERSION 8.10.0
RUN git clone git://github.com/creationix/nvm.git /opt/nvm
# nvmでnodeとnpmをインストール
RUN source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
&& nvm use default
# nodeとnpmのパスを通す
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# # Python3, pip
# RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm
# RUN yum install -y python36
# RUN yum install -y python36u-pip
# RUN yum install -y python-pip
#
# # NeoVim
# ADD lib/neovim.repo /etc/yum.repos.d/
# RUN yum install -y neovim
# RUN echo "alias vi='/bin/nvim'" >> /root/.bashrc
# RUN python3.6 -m pip install neovim
# RUN python2.7 -m pip install neovim

# WorkingDirectory作成
RUN mkdir -p /work
WORKDIR /work
